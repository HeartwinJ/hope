import 'package:flutter/foundation.dart';

class Job {
  final String title;
  final String description;
  final String name;
  final String address;
  final String phone;
  final String email;
  final double lat;
  final double long;
  final String postedBy;
  final String id;

  Job(
      {@required this.title,
      @required this.description,
      @required this.name,
      @required this.address,
      @required this.phone,
      @required this.email,
      @required this.lat,
      @required this.long,
      this.postedBy,
      this.id});
}
