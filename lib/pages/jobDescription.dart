import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hope/widgets/drawerContents.dart';
import 'package:hope/widgets/header.dart';
import 'package:url_launcher/url_launcher.dart';

class JobDescription extends StatefulWidget {
  final String uid;
  final String title;
  final double lat;
  final double long;
  final String phone;
  final String description;

  JobDescription({
    this.uid,
    this.title,
    this.lat,
    this.long,
    this.phone,
    this.description,
  });

  @override
  _JobDescriptionState createState() => _JobDescriptionState();
}

class _JobDescriptionState extends State<JobDescription> {
  GoogleMapController mapController;
  final Set<Marker> _markers = new Set();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  // _launchURL() async {
  //   var url = widget.phone;
  //   try {
  //     if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  //   }
  //   catch(e) {
  //     print(e);
  //   }
  // }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  Set<Marker> myMarker() {
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId(LatLng(widget.lat, widget.long).toString()),
        position: LatLng(widget.lat, widget.long),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });

    return _markers;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
               Header(
                  title: widget.title,
                  onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
                ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // Padding(
                    //   padding: const EdgeInsets.symmetric(vertical: 10),
                    //   child: Text(
                    //     widget.title,
                    //     style: TextStyle(
                    //       fontSize: 40,
                    //       fontWeight: FontWeight.w700,
                    //     ),
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                        widget.description,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height / 2,
                      child: GoogleMap(
                        onMapCreated: _onMapCreated,
                        initialCameraPosition: CameraPosition(
                          target: LatLng(widget.lat, widget.long),
                          zoom: 12.0,
                        ),
                        markers: this.myMarker(),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                      decoration: BoxDecoration(border: Border.all(),),
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.phone_android),
                          FlatButton(
                            onPressed: () => launch("tel:${widget.phone}"),
                            child: Text(
                              widget.phone,
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.w600,
                                color: Colors.deepOrangeAccent,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
            ],
          ),
        ),
        key: _scaffoldKey,
        endDrawer: Drawer(child: DrawerContents()));
  }
}
