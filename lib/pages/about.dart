import 'package:flutter/material.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/widgets/drawerContents.dart';
import 'package:hope/widgets/header.dart';

class About extends StatelessWidget {
  final String version;
  About(this.version);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Header(
                  title: AppLocalizations.of(context).translate('about'),
                  onPressed: () => _scaffoldKey.currentState.openEndDrawer()),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Image(
                          image: AssetImage('images/titleless.png'),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(40.0),
                          child: Text(
                            AppLocalizations.of(context).translate('HOPE'),
                            style: TextStyle(
                              color: Colors.deepOrange,
                              fontSize: 30,
                            ),
                          ),
                        ),
                        Text(
                          'The Hopeful Platform for Employment is an online portal that aims to help connect migrant workers displaced as a result of the Covid-19 pandemic with businesses looking to employ workers within the same geographical region.',
                          style: TextStyle(fontSize: 16),
                          textAlign: TextAlign.justify,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Text(
                            'v$version',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(AppLocalizations.of(context).translate('with_love'),
                    style:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
              ),
            ],
          ),
        ),
        key: _scaffoldKey,
        endDrawer: Drawer(child: DrawerContents()));
  }
}
