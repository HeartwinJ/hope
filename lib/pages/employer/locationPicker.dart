import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hope/services/firestoreService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';

class LocationPicker extends StatefulWidget {
  final String uid;
  final String title;
  final String description;
  final String name;
  final String address;
  final String phoneNum;
  final String email;

  LocationPicker({
    @required this.uid,
    @required this.title,
    @required this.description,
    @required this.name,
    @required this.address,
    @required this.phoneNum,
    @required this.email,
  });
  @override
  _LocationPickerState createState() => _LocationPickerState();
}

class _LocationPickerState extends State<LocationPicker> {
  Position _currentPosition;
  double _latitude = 21.494069;
  double _longitude = 78.456259;
  GoogleMapController mapController;
  LatLng _temp;
  Map<String, double> userLocation;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              AppLocalizations.of(context).translate('add_location'),
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 2,
                  child: GoogleMap(
                    onMapCreated: _onMapCreated,
                    onCameraMove: (object) {
                      _temp = object.target;
                    },
                    initialCameraPosition: CameraPosition(
                      target: LatLng(_latitude, _longitude),
                      zoom: 4.5,
                    ),
                  ),
                ),
                Icon(
                  Icons.center_focus_strong,
                  color: Colors.red,
                  size: 30,
                ),
              ],
            ),
          ),
          BorderButton(
            label: AppLocalizations.of(context).translate('map_location'),
            onPressed: () {
              _latitude = _temp.latitude;
              _longitude = _temp.longitude;
              FirestoreService _store = FirestoreService();
              _store.postJob(widget.uid, widget.title, widget.description, widget.name, widget.address,
                  widget.phoneNum, widget.email, _latitude, _longitude);
              Navigator.pop(context);
              Navigator.pop(context);
            },
          ),
          BorderButton(
            label: AppLocalizations.of(context).translate('current_location'),
            onPressed: () {
              _getCurrentLocation();
              _latitude = _currentPosition.latitude;
              _longitude = _currentPosition.longitude;
              FirestoreService _store = FirestoreService();
              _store.postJob(widget.uid, widget.title, widget.description, widget.name, widget.address,
                  widget.phoneNum, widget.email, _latitude, _longitude);
              Navigator.pop(context);
              Navigator.pop(context);
            },
          ),
        ],
      ),
    ));
  }
}
