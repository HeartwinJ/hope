import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';

class EmployerRegistrationForm extends StatefulWidget {
  @override
  _EmployerRegistrationFormState createState() =>
      _EmployerRegistrationFormState();
}

class _EmployerRegistrationFormState extends State<EmployerRegistrationForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  String _name;
  String _address;
  String _phoneNum;
  String _aadharNum;

  void _vaildateRegistration() async {
    AuthService _authService = AuthService();
    final FormState _form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      _form.save();
      await _authService
          .signUpEmployerWithGoogle(_name, _address, _phoneNum, _aadharNum)
          .then((user) => {
                if (user != null)
                  {Navigator.popUntil(context, (route) => route.isFirst)}
                else
                  {
                    // TODO: Handle the errors properly
                    print('An Error Occured!')
                  }
              });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: _formKey,
        autovalidate: _autovalidate,
        child: Column(
          children: <Widget>[
            Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text(
                      AppLocalizations.of(context).translate('employer_registration'),
                      style: TextStyle(
                        fontSize: 26,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
            Expanded(
                      child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('name'),
                      ),
                      onSaved: (input) {
                        _name = input;
                      },
                      validator: (value) =>
                          (value.isNotEmpty) ? null : 'Enter a Name',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('address'),
                      ),
                      onSaved: (input) {
                        _address = input;
                      },
                      validator: (value) =>
                          (value.isNotEmpty) ? null : 'Enter a Address',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('phone_no'),
                      ),
                      onSaved: (input) {
                        _phoneNum = '+91' + input;
                      },
                      validator: (value) =>
                          (value.length == 10) ? null : 'Enter a Valid Phone Number',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('aadhar'),
                      ),
                      onSaved: (input) {
                        _aadharNum = input;
                      },
                      validator: (value) => (value.length == 12)
                          ? null
                          : 'Enter your 12-Digit Aadhar Number',
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: BorderButton(
                label: AppLocalizations.of(context).translate('submit'),
                onPressed: _vaildateRegistration,
              ),
            ),
          ],
        ),
        
      ),
    ));
  }
}
