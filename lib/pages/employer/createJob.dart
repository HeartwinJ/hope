import 'package:flutter/material.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/pages/employer/locationPicker.dart';

class CreateJob extends StatefulWidget {
  final String uid;

  String title;
  String description;
  String name;
  String address;
  String phoneNum;
  String email;
  String postedBy;

  CreateJob({
    this.uid,
    this.title,
    this.description,
    this.name,
    this.address,
    this.phoneNum,
    this.email,
  });

  @override
  _CreateJobState createState() => _CreateJobState();
}

class _CreateJobState extends State<CreateJob> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController controller;
  bool autovalidate = false;

  // String title;
  // String description;
  // String name;
  // String address;
  // String phoneNum;
  // String email;

  String _emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty) return '*Required';
    if (!regex.hasMatch(value))
      return '*Enter a valid email';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: formKey,
        autovalidate: autovalidate,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                AppLocalizations.of(context).translate('create_job'),
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                controller: TextEditingController(text: widget.title),
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).translate('title'),
                ),
                onSaved: (input) => widget.title = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                controller: TextEditingController(text: widget.description),
                decoration: InputDecoration(
                  labelText:
                      AppLocalizations.of(context).translate('description'),
                ),
                onSaved: (input) => widget.description = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                controller: TextEditingController(text: widget.name),
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).translate('name'),
                ),
                onSaved: (input) => widget.name = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                controller: TextEditingController(text: widget.address),
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).translate('address'),
                ),
                onSaved: (input) => widget.address = input,
                validator: (value) => (value.isNotEmpty)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                controller: TextEditingController(text: widget.phoneNum),
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).translate('phone_no'),
                ),
                onSaved: (input) => widget.phoneNum = input,
                validator: (value) => (value.length >= 10 && value.length <= 13)
                    ? null
                    : AppLocalizations.of(context).translate('required'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextFormField(
                controller: TextEditingController(text: widget.email),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).translate('email'),
                ),
                onSaved: (input) => widget.email = input,
                validator: _emailValidator,
              ),
            ),
            BorderButton(
              label: AppLocalizations.of(context).translate('add_location'),
              onPressed: () {
                if (formKey.currentState.validate()) {
                  formKey.currentState.save();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LocationPicker(
                                uid: widget.uid,
                                title: widget.title,
                                description: widget.description,
                                name: widget.name,
                                address: widget.address,
                                phoneNum: widget.phoneNum,
                                email: widget.email,
                              )));
                }
              },
            ),
          ],
        ),
      ),
    ));
  }
}
