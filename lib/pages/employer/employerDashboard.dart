import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hope/models/job.dart';
import 'package:hope/pages/employer/createJob.dart';
import 'package:hope/pages/jobDescription.dart';
import 'package:hope/services/firestoreService.dart';
import 'package:hope/widgets/drawerContents.dart';
import 'package:hope/widgets/header.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/widgets/jobListingBig.dart';
import 'package:provider/provider.dart';

class EmployerDashboard extends StatefulWidget {
  final String uid;

  EmployerDashboard(this.uid);

  @override
  _EmployerDashboardState createState() => _EmployerDashboardState();
}

class _EmployerDashboardState extends State<EmployerDashboard> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirestoreService fst = FirestoreService();
  String _uid;

  @override
  void initState() {
    super.initState();
    uidCall(); // here you write the codes to input the data into firestore
  }

  void uidCall() async {
    final FirebaseUser user = await auth.currentUser();
    _uid = user.uid;
    print('UID is $_uid');
  }

  @override
  Widget build(BuildContext context) {
    final jobs = Provider.of<List<Job>>(context);

    print(widget.uid);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Header(
              title: AppLocalizations.of(context).translate('my_listings'),
              onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: jobs.length,
                itemBuilder: (context, index) {
                  return _uid == jobs[index].postedBy
                      ? JobListingBig(
                          title: jobs[index].title,
                          description: jobs[index].description,
                          location: jobs[index].address,
                          buttonPress: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => JobDescription(
                                  uid: widget.uid,
                                  title: jobs[index].title,
                                  description: jobs[index].description,
                                  phone: jobs[index].phone,
                                  lat: jobs[index].lat,
                                  long: jobs[index].long,
                                ),
                              ),
                            );
                          },
                          edit: () {
                            fst.deleteJob(jobs[index].id);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CreateJob(
                                  uid: this.widget.uid,
                                  title: jobs[index].title,
                                  description: jobs[index].description,
                                  name: jobs[index].name,
                                  address: jobs[index].address,
                                  phoneNum: jobs[index].phone,
                                  email: jobs[index].email,
                                ),
                              ),
                            );
                          },
                          delete: () => fst.deleteJob(jobs[index].id),
                        )
                      : Container(child: null);
                },
              ),
            ),
            Container(padding: EdgeInsets.symmetric(vertical: 40),)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('Create job'),
        icon: Icon(Icons.add),
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CreateJob(uid: this.widget.uid),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      key: _scaffoldKey,
      endDrawer: Drawer(
        child: DrawerContents(),
      ),
    );
  }
}
