import 'package:flutter/material.dart';
import 'package:hope/pages/employee/employeeRegistrationForm.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/pages/employer/employerRegistrationForm.dart';

class ChoosePage extends StatefulWidget {
  @override
  _ChoosePageState createState() => _ChoosePageState();
}

class _ChoosePageState extends State<ChoosePage> {
  @override
  Widget build(BuildContext context) {
    final Brightness brightness = MediaQuery.of(context).platformBrightness;
    bool isDark = brightness == Brightness.dark;

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 2,
            child: FlatButton(
              child: Text(
                AppLocalizations.of(context).translate('seek'),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: Colors.deepOrange,
                ),
              ),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EmployeeRegistrationForm(),
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 2,
            decoration: BoxDecoration(
              color: Colors.deepOrange,
            ),
            child: FlatButton(
              child: Text(
                AppLocalizations.of(context).translate('offer'),
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: isDark ? Color(0xFF303030) : Colors.white,
                ),
              ),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EmployerRegistrationForm(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
