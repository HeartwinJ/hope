import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/pages/choosePage.dart';

final GoogleSignIn googleSignIn = GoogleSignIn();

class Authenticator extends StatefulWidget {
  @override
  _AuthenticatorState createState() => _AuthenticatorState();
}

class _AuthenticatorState extends State<Authenticator> {
  bool _isAuth = false;

  @override
  void initState() {
    super.initState();
    googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      if (account != null) {
        print('Signed in! $account');
        setState(() {
          _isAuth = true;
        });
      } else {
        setState(() {
          _isAuth = false;
        });
      }
    });
  }

  _signIn() {
    googleSignIn.signIn();
  }

  Scaffold _getStarted(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.center,
                    child: Image(
                      image: AssetImage('images/titleless.png'),
                    ),
                  ),
                ),
                BorderButton(
                  label: AppLocalizations.of(context).translate('get_started'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ChoosePage()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Scaffold _login(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.center,
                    child: Image(
                      image: AssetImage('images/titleless.png'),
                    ),
                  ),
                ),
                BorderButton(
                  label: AppLocalizations.of(context).translate('login'),
                  onPressed: _signIn(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _isAuth ? _getStarted(context) : _login(context);
  }
}
