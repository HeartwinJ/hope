import 'package:flutter/material.dart';
import 'package:hope/models/job.dart';
import 'package:hope/pages/jobDescription.dart';
import 'package:hope/widgets/jobListing.dart';
import 'package:provider/provider.dart';
import 'package:hope/models/app_localizations.dart';

class Search extends StatefulWidget {
  final String uid;
  Search({@required this.uid});

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  TextEditingController controller = TextEditingController();
  String _searchTerm = '';
  List<Job> filtered;

  @override
  Widget build(BuildContext context) {
    final jobs = Provider.of<List<Job>>(context);

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                controller: controller,
                onChanged: (input) {
                  setState(() {
                    _searchTerm = controller.text;
                  });
                  print(controller.text);
                },
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  labelText: AppLocalizations.of(context).translate('search'),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Wrap(
                spacing: 10,
                children: <Widget>[
                  ChoiceChip(
                    selected: false,
                    backgroundColor: Colors.deepOrange,
                    label: Text('Electrician'),
                    onSelected: (selected) {
                      setState(() {
                        _searchTerm = 'electrician';
                        print(_searchTerm);
                      });
                    },
                  ),
                  ChoiceChip(
                    selected: false,
                    backgroundColor: Colors.deepOrange,
                    label: Text('Carpenter'),
                    onSelected: (selected) {
                      setState(() {
                        _searchTerm = 'carpenter';
                        print(_searchTerm);
                      });
                    },
                  ),
                  ChoiceChip(
                    selected: false,
                    backgroundColor: Colors.deepOrange,
                    label: Text('Driver'),
                    onSelected: (selected) {
                      setState(() {
                        _searchTerm = 'driver';
                        print(_searchTerm);
                      });
                    },
                  ),
                  ChoiceChip(
                    selected: false,
                    backgroundColor: Colors.deepOrange,
                    label: Text('Painter'),
                    onSelected: (selected) {
                      setState(() {
                        _searchTerm = 'painter';
                        print(_searchTerm);
                      });
                    },
                  ),
                  ChoiceChip(
                    selected: false,
                    backgroundColor: Colors.deepOrange,
                    label: Text('construction worker'),
                    onSelected: (selected) {
                      setState(() {
                        _searchTerm = 'construction worker';
                        print(_searchTerm);
                      });
                    },
                  ),
                ],
              ),
            ),
            Container(
              child: Expanded(
                  child: ListView.builder(
                itemCount: jobs.length,
                itemBuilder: (context, index) {
                  return jobs[index].title.toLowerCase().startsWith(_searchTerm)
                      ? JobListing(
                          job: jobs[index],
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => JobDescription(
                                  uid: widget.uid,
                                  title: jobs[index].title,
                                  description: jobs[index].description,
                                  phone: jobs[index].phone,
                                  lat: jobs[index].lat,
                                  long: jobs[index].long,
                                ),
                              ),
                            );
                          },
                        )
                      : Container(child: null);
                },
              )),
            )
          ],
        ),
      ),
    );
  }
}
