import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';

class EmployeeLoginPage extends StatefulWidget {
  @override
  _EmployerLoginPageState createState() => _EmployerLoginPageState();
}

class _EmployerLoginPageState extends State<EmployeeLoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  /* String _phoneNum;
  String _verificationId;
  String _otp; */

  /* Future _sendOTPToPhoneNumber() async {
    final PhoneVerificationCompleted verificationCompleted = (value) {
      print('Phone Verification Completed');
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      print(
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      _verificationId = verificationId;
      print("OTP sent to " + _phoneNum);
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
      print("Timeout");
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: _phoneNum,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  } */

  void _validateLogIn() async {
    AuthService _authService = AuthService();
    final FormState _form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      _form.save();
      try {
        await _authService
          .signInWithGoogle()
          .then((user) => {
                if (user != null)
                  {Navigator.popUntil(context, (route) => route.isFirst)}
                else
                  {print('An error Occured!')}
              });
      } catch (error) {
        print(error);
      }
    }
  }

  /* Widget _otpVerBuilder() {
    GlobalKey<FormState> formKey = GlobalKey();
    bool autovalidate = false;
    return Container(
      height: MediaQuery.of(context).size.height / 5,
      child: Form(
        key: formKey,
        autovalidate: autovalidate,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Center(
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'OTP',
                  ),
                  onSaved: (input) {
                    _otp = input;
                    print(_otp);
                  },
                  validator: (input) => (input.isEmpty)
                      ? AppLocalizations.of(context).translate('required')
                      : null,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: BorderButton(
                label: AppLocalizations.of(context).translate('login'),
                onPressed: () {
                  if (formKey.currentState.validate()) {
                    formKey.currentState.save();
                    try {
                      _validateLogIn();
                    } catch (error) {
                      print(error);
                    }
                  }
                },
              ),
            ),
            FlatButton(
              child: Text(
                AppLocalizations.of(context).translate('resend'),
                style: TextStyle(
                  color: Colors.deepOrange,
                ),
              ),
              onPressed: () {
                try {
                  _sendOTPToPhoneNumber();
                } catch (error) {
                  print(error);
                }
              },
            ),
          ],
        ),
      ),
    );
  } */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 100),
            width: double.infinity,
            child: Center(
              child: Text(
                AppLocalizations.of(context).translate('great_to'),
                style: TextStyle(
                  fontSize: 40,
                  fontFamily: 'Montserrat',
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            child: Form(
              key: _formKey,
              autovalidate: _autovalidate,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    /* Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 20),
                      child: TextFormField(
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate('phone_no'),
                        ),
                        onSaved: (input) {
                          _phoneNum = '+91' + input;
                          print(_phoneNum);
                        },
                        validator: (value) =>
                            (value.length == 10)? null : AppLocalizations.of(context).translate('valid_phone'),
                      ),
                    ), */
                    BorderButton(
                      label: AppLocalizations.of(context).translate('login'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          try {
                            _validateLogIn();
                          } catch (error) {
                            print(error);
                          }
                          /* showDialog(
                              context: context,
                              child: AlertDialog(content: _otpVerBuilder())); */
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
