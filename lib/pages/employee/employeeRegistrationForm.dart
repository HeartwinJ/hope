import 'package:flutter/material.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/borderButton.dart';
import 'package:hope/models/app_localizations.dart';

class EmployeeRegistrationForm extends StatefulWidget {
  @override
  _EmployeeRegistrationFormState createState() =>
      _EmployeeRegistrationFormState();
}

class _EmployeeRegistrationFormState extends State<EmployeeRegistrationForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;

  String _name;
  String _occupation;
  String _phoneNum;
  String _aadharNum;
  String _prevEmployerName;
  String _prevEmployerNum;

  void _vaildateRegistration() async {
    AuthService _authService = AuthService();
    final FormState _form = _formKey.currentState;
    if (_formKey.currentState.validate()) {
      _form.save();
      await _authService
          .signUpEmployeeWithGoogle(_name, _occupation, _phoneNum, _aadharNum,
              _prevEmployerName, _prevEmployerNum)
          .then((user) => {
                if (user != null)
                  {Navigator.popUntil(context, (route) => route.isFirst)}
                else
                  {
                    // TODO: Handle the errors properly
                    print('An Error Occured!')
                  }
              });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Form(
        key: _formKey,
        autovalidate: _autovalidate,
        child: Column(
          children: <Widget>[
            Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text(
                      AppLocalizations.of(context).translate('employee_registration'),
                      style: TextStyle(
                        fontSize: 26,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
            Expanded(
                      child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('name'),
                      ),
                      onSaved: (input) {
                        _name = input;
                      },
                      validator: (input) => (input.isEmpty)
                          ? AppLocalizations.of(context).translate('required')
                          : null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('occupation'),
                      ),
                      onSaved: (input) {
                        _occupation = input;
                      },
                      validator: (input) => (input.isEmpty)
                          ? AppLocalizations.of(context).translate('required')
                          : null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('phone_no'),
                      ),
                      onSaved: (input) {
                        _phoneNum = '+91' + input;
                      },
                      validator: (input) => (input.length == 10)
                          ? null
                          : AppLocalizations.of(context).translate('valid_phone'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: AppLocalizations.of(context).translate('aadhar'),
                      ),
                      onSaved: (input) {
                        _aadharNum = input;
                      },
                      validator: (input) => (input.length != 12)
                          ? AppLocalizations.of(context).translate('valid_aadhar')
                          : null,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('prev_emp_name'),
                      ),
                      onSaved: (input) {
                        _prevEmployerName = input;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText:
                            AppLocalizations.of(context).translate('prev_emp_phone'),
                      ),
                      onSaved: (input) {
                        _prevEmployerNum = '+91' + input;
                      },
                      validator: (input) => (input.isEmpty || input.length == 10)
                          ? null
                          : AppLocalizations.of(context).translate('valid_phone'),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: BorderButton(
                      label: AppLocalizations.of(context).translate('submit'),
                      onPressed: _vaildateRegistration,
                    ),
            ),
          ],
        ),
      ),
    ));
  }
}
