import 'package:flutter/material.dart';
import 'package:hope/models/job.dart';
import 'package:hope/pages/jobDescription.dart';
import 'package:hope/widgets/jobListing.dart';
import 'package:provider/provider.dart';

class JobsWrapper extends StatefulWidget {
  final String uid;

  JobsWrapper({this.uid});

  @override
  _JobsWrapperState createState() => _JobsWrapperState();
}

class _JobsWrapperState extends State<JobsWrapper> {
  
  
  
  @override
  Widget build(BuildContext context) {
    final jobs = Provider.of<List<Job>>(context);
    return ListView.builder(
      itemCount: jobs.length,
      itemBuilder: (context, index) {
        return JobListing(
          job: jobs[index],
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => JobDescription(
                  uid: widget.uid,
                  title: jobs[index].title,
                  description: jobs[index].description,
                  phone: jobs[index].phone,
                  lat: jobs[index].lat,
                  long: jobs[index].long,
                ),
              ),
            );
          },
        );
      },
    );
  }
}
