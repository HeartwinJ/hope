import 'package:flutter/material.dart';
import 'package:hope/pages/employee/employeeJobsWrapper.dart';
import 'package:hope/widgets/drawerContents.dart';
import 'package:hope/models/app_localizations.dart';

import 'search.dart';

class EmployeeDashboard extends StatelessWidget {
  final String uid;

  EmployeeDashboard(this.uid);

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        AppLocalizations.of(context).translate('job_listings'),
                        style: TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: Icon(Icons.search, size: 26),
                        onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Search(uid: this.uid),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: Icon(Icons.menu, size: 26),
                        onPressed: () =>
                            _scaffoldKey.currentState.openEndDrawer(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: JobsWrapper(uid: this.uid),
            ),
          ],
        ),
      ),
      key: _scaffoldKey,
      endDrawer: Drawer(
        child: DrawerContents(),
      ),
    );
  }
}
