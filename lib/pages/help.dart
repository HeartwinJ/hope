import 'package:flutter/material.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/widgets/drawerContents.dart';
import 'package:hope/widgets/header.dart';

class Help extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
              child: Header(
          title: AppLocalizations.of(context).translate('helpline'),
          onPressed: () => _scaffoldKey.currentState.openEndDrawer()),
      ),
        key: _scaffoldKey,
      endDrawer: Drawer(child: DrawerContents())
    );
  }
}