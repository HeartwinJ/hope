import 'package:flutter/material.dart';

class BorderButton extends StatelessWidget {
  final String label;
  final Function onPressed;

  @override
  BorderButton({@required this.label, @required this.onPressed});

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      width: double.infinity,
      height: 40,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.deepOrange),
        borderRadius: BorderRadius.circular(10),
      ),
      child: FlatButton(
        child: Text(
          this.label,
          style: TextStyle(
            fontSize: 16,
          ),
        ),
        onPressed: this.onPressed,
      ),
    );
  }
}
