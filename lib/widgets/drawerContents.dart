import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:hope/pages/about.dart';
import 'package:hope/models/app_localizations.dart';
import 'package:hope/pages/help.dart';
import 'package:hope/services/authService.dart';
import 'package:hope/widgets/drawerItem.dart';

class DrawerContents extends StatefulWidget {
  @override
  _DrawerContentsState createState() => _DrawerContentsState();
}

class _DrawerContentsState extends State<DrawerContents> {
  String version = '1.0.2'; //TODO: Enter version number here on release

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: IconButton(
                    icon: Icon(
                      Icons.close,
                      size: 30,
                    ),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 30),
            alignment: Alignment.center,
            child: Image(
              image: AssetImage('images/titleless.png'),
            ),
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                /* DrawerItem(
                  icon: Icons.search,
                  label: AppLocalizations.of(context).translate('search'),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => About(),
                    ),
                  ),
                ), */
                DrawerItem(
                  icon: Icons.home,
                  label: AppLocalizations.of(context).translate('dashboard'),
                  onPressed: () {
                    Navigator.popUntil(context, (route) => route.isFirst);
                    Navigator.of(context).pop();
                  },
                ),
                // DrawerItem(
                //   icon: Icons.settings,
                //   label: AppLocalizations.of(context).translate('settings'),
                //   onPressed: () => Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //       builder: (context) => Settings(),
                //     ),
                //   ),
                // ),
                // DrawerItem(
                //   icon: Icons.language,
                //   label: AppLocalizations.of(context).translate('language'),
                //   onPressed: null,
                // ),
                // DrawerItem(
                //   icon: Icons.brightness_medium,
                //   label: AppLocalizations.of(context).translate('theme'),
                //   onPressed: () => showDialog(
                //     context: context,
                //     child: AlertDialog(
                //       content: Text(
                //         'Choose Theme',
                //         style: TextStyle(
                //           fontFamily: 'Raleway',
                //           fontWeight: FontWeight.bold,
                //           fontSize: 20,
                //         ),
                //       ),
                //       actions: <Widget>[
                //         FlatButton(
                //           child: Text(
                //             'Light',
                //             style: TextStyle(
                //               color: Colors.deepOrange,
                //               fontWeight: FontWeight.bold,
                //             ),
                //           ),
                //           onPressed: () => DynamicTheme.of(context)
                //               .setBrightness(Theme.of(context).brightness ==
                //                       Brightness.light
                //                   ? Brightness.dark
                //                   : Brightness.light),
                //         ),
                //         FlatButton(
                //           child: Text(
                //             'Dark',
                //             style: TextStyle(
                //               color: Colors.deepOrange,
                //               fontWeight: FontWeight.bold,
                //             ),
                //           ),
                //           onPressed: () => DynamicTheme.of(context)
                //               .setBrightness(Theme.of(context).brightness ==
                //                       Brightness.dark
                //                   ? Brightness.light
                //                   : Brightness.dark),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
                DrawerItem(
                  icon: Icons.info,
                  label: AppLocalizations.of(context).translate('about'),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => About(version),
                    ),
                  ),
                ),
                DrawerItem(
                  icon: Icons.help,
                  label: AppLocalizations.of(context).translate('helpline'),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Help(),
                    ),
                  ),
                ),
                DrawerItem(
                  icon: Icons.exit_to_app,
                  label: AppLocalizations.of(context).translate('logout'),
                  color: Colors.red,
                  onPressed: () {
                    AuthService _authService = AuthService();
                    _authService.signOut();
                  },
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              'v$version',
              style: TextStyle(color: Colors.deepOrange),
            ),
          ),
        ],
      ),
    );
  }
}
