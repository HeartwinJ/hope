import 'package:flutter/material.dart';
import 'package:hope/models/user.dart';
import 'package:hope/models/userTypes.dart';
import 'package:hope/pages/employee/employeeDashboard.dart';
import 'package:hope/pages/employer/employerDashboard.dart';
import 'package:hope/services/authService.dart';
import 'package:provider/provider.dart';
import 'package:hope/pages/welcomePage.dart';

class AuthWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    var userType;
    AuthService _auth = AuthService();
    _auth.getUserType(user).then((res) => userType = res);

    return FutureBuilder(
      future: _auth.getUserType(user).then((res) => userType = res),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (user != null && userType == UserTypes.EMPLOYEE) {
            return EmployeeDashboard(user.uid);
          } else if (user != null && userType == UserTypes.EMPLOYER) {
            return EmployerDashboard(user.uid);
          } else {
            return WelcomePage();
          }
        } else {
          return Scaffold(
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Center(
                child: Image(
                  image: AssetImage('images/titleless.png'),
                ),
              ),
            ),
          );
        }
      },
    );
  }
}
