import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hope/models/user.dart';
import 'package:hope/services/firestoreService.dart';
import 'package:hope/models/userTypes.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirestoreService _store = FirestoreService();

  //Create User from FirebaseUser
  User _createUser(FirebaseUser user) {
    return (user != null) ? User(user.uid) : null;
  }

  //Get userType from UID
  Future<UserTypes> getUserType(User user) async {
    String userType;
    try {
      await _store
          .getUserDocument(user.uid)
          .then((doc) => {userType = doc.data['userType']});
    } catch (error) {
      print(error);
    }
    return (userType == 'EMPLOYER') ? UserTypes.EMPLOYER : UserTypes.EMPLOYEE;
  }

  //Auth Change User Stream
  Stream<User> get userStream {
    return _auth.onAuthStateChanged
        .map((FirebaseUser user) => _createUser(user));
  }

  // Sign Up Employer
  /* Future signUpEmployer(
      String name,
      String address,
      String phoneNum,
      String aadharNum,
      String email,
      String password,
      UserTypes userType) async {
    try {
      FirebaseUser user;
      await _auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((authResult) => {
                user = authResult.user,
                _store.updateEmployer(user.uid, name, address, phoneNum,
                    aadharNum, email, userType)
              });
      return _createUser(user);
    } catch (error) {
      print(error.toString());
      return null;
    }
  } */

  // Sign Up Employee
  /* Future signUpEmployee(
      String name,
      String occupation,
      String phoneNum,
      String aadharNum,
      String prevEmployerName,
      String prevEmployerNum,
      UserTypes userType,
      String verificationId,
      String smsCode) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
          verificationId: verificationId, smsCode: smsCode);
      FirebaseUser user;
      await _auth
          .signInWithCredential(credential)
          .then((authResult) => {
                user = authResult.user,
                _store.updateEmployee(user.uid, name, occupation, phoneNum,
                    aadharNum, prevEmployerName, prevEmployerNum, userType)
              })
          .catchError((error) => print(error));
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  } */

  // Sign In Employer
  Future signInEmployer(String email, String password) async {
    try {
      FirebaseUser user;
      await _auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((authResult) => {user = authResult.user});
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // Sign In Employee
  /* Future signInEmployee(
      String phoneNum, String verificationId, String smsCode) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
          verificationId: verificationId, smsCode: smsCode);
      FirebaseUser user;
      await _auth
          .signInWithCredential(credential)
          .then((authResult) => {user = authResult.user})
          .catchError((error) => print(error));
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  } */

  // Google Employee Sign Up
  Future signUpEmployeeWithGoogle(
      String name,
      String occupation,
      String phoneNum,
      String aadharNum,
      String prevEmployerName,
      String prevEmployerNum) async {
    try {
      final GoogleSignIn _googleSignIn = GoogleSignIn();
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      FirebaseUser user;
      await _auth
          .signInWithCredential(credential)
          .then((authResult) => {
                user = authResult.user,
                _store.updateEmployee(
                    user.uid,
                    name,
                    occupation,
                    phoneNum,
                    aadharNum,
                    prevEmployerName,
                    prevEmployerNum,
                    UserTypes.EMPLOYEE)
              })
          .catchError((error) => print(error));
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // Google Employer Sign Up
  Future signUpEmployerWithGoogle(
      String name, String address, String phoneNum, String aadharNum) async {
    try {
      final GoogleSignIn _googleSignIn = GoogleSignIn();
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      FirebaseUser user;
      await _auth
          .signInWithCredential(credential)
          .then((authResult) => {
                user = authResult.user,
                _store.updateEmployer(user.uid, name, address, phoneNum,
                    aadharNum, UserTypes.EMPLOYER)
              })
          .catchError((error) => print(error));
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // Check of given user already exists
  Future<bool> alreadyExists(FirebaseUser user) async {
    bool returnVal = false;
    await _store.getUserDocument(user.uid).then((doc) => {
          if (doc == null) {returnVal = false} else {returnVal = true}
        });
    return returnVal;
  }

  // Google Sign In
  Future signInWithGoogle() async {
    try {
      final GoogleSignIn _googleSignIn = GoogleSignIn();
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      FirebaseUser user;
      await _auth
          .signInWithCredential(credential)
          .then((authResult) => {
                alreadyExists(authResult.user).then((res) => {
                      if (res) {user = authResult.user} else {user = null}
                    })
              })
          .catchError((error) => print(error));
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // Sign Out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error);
      return null;
    }
  }
}
