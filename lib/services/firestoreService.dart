import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hope/models/job.dart';
import 'package:hope/models/userTypes.dart';

class FirestoreService {
  final CollectionReference _users = Firestore.instance.collection('users');
  final CollectionReference _jobs = Firestore.instance.collection('jobs');

  Future updateEmployer(String uid, String name, String address,
      String phoneNum, String aadharNum, UserTypes userType) async {
    try {
      _users.document(uid).setData({
        'name': name,
        'address': address,
        'phoneNum': phoneNum,
        'aadharNum': aadharNum,
        'userType': (userType == UserTypes.EMPLOYER) ? 'EMPLOYER' : 'EMPLOYEE'
      });
    } catch (error) {
      print(error.toString());
    }
  }

  Future updateEmployee(
      String uid,
      String name,
      String occupation,
      String phoneNum,
      String aadharNum,
      String prevEmployerName,
      String prevEmployerNum,
      UserTypes userType) async {
    try {
      _users.document(uid).setData({
        'name': name,
        'occupation': occupation,
        'phoneNum': phoneNum,
        'aadharNum': aadharNum,
        'prevEmployerName': prevEmployerName,
        'prevEmployerNum': prevEmployerNum,
        'userType': (userType == UserTypes.EMPLOYER) ? 'EMPLOYER' : 'EMPLOYEE'
      });
    } catch (error) {
      print(error.toString());
    }
  }

  Future<DocumentSnapshot> getUserDocument(String uid) async {
    DocumentSnapshot doc;
    try {
      await _users.document(uid).get().then((result) => doc = result);
    } catch (error) {
      print(error.toString());
    }
    return doc;
  }

  Future postJob(
      String uid,
      String title,
      String description,
      String name,
      String address,
      String phoneNum,
      String email,
      double lat,
      double long) async {
    try {
      await _jobs.add({
        'postedBy': uid,
        'title': title,
        'description': description,
        'name': name,
        'address': address,
        'phoneNum': phoneNum,
        'email': email,
        'latitude': lat,
        'longitude': long
      });
    } catch (error) {
      print(error.toString());
    }
  }

  List<Job> _createJobFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Job(
          title: doc.data['title'],
          description: doc.data['description'],
          name: doc.data['name'],
          address: doc.data['address'],
          phone: doc.data['phoneNum'],
          email: doc.data['email'],
          lat: doc.data['latitude'],
          long: doc.data['longitude'],
          postedBy: doc.data['postedBy'],
          id: doc.documentID);
    }).toList();
  }

  Stream<List<Job>> get jobs {
    return _jobs
        .snapshots()
        .map((snapshot) => _createJobFromSnapshot(snapshot));
  }

  Future deleteJob(String jobID) async {
    return await _jobs.document(jobID).delete();
  }

  Future updateJob(
      String uid,
      String title,
      String description,
      String name,
      String address,
      String phoneNum,
      String email,
      double lat,
      double long,
      String jobID) async {
    try {
      await _jobs.document(jobID).updateData({
        'postedBy': uid,
        'title': title,
        'description': description,
        'name': name,
        'address': address,
        'phoneNum': phoneNum,
        'email': email,
        'latitude': lat,
        'longitude': long
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
